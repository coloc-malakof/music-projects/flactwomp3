#################################################
# Authors
#   Coloc Malakof <colocmalakof@gmail.com>
# Description
#   This is an optimized flactwomp3 docker image.
# Documentation
#   https://gitlab.com/colocmalakof/flactwomp3
#################################################

# Baseline
FROM alpine:edge as mp3gain

RUN set -xe && \
    # Install required packages
    apk add --no-cache \
        build-base \
        mpg123-dev \
        unzip && \
    wget "https://downloads.sourceforge.net/project/mp3gain/mp3gain/1.6.2/mp3gain-1_6_2-src.zip" && \
    unzip mp3gain-*.zip && \
    make

FROM alpine:edge

# Metadata
LABEL Maintainer="colocmalakof" \
    Version="1.0" \
    Description="Optimized flactwomp3 docker image."

# Environment variables for the container
ENV UID=1000 \
    GID=1000

# Configure the system
RUN set -xe && \
    # Install required packages
    apk add --no-cache \
        flac \
        lame \
        mpg123-dev \
        python3 \
        py3-eyed3 \
        py3-pip && \
    pip3 install --no-cache \
        ruamel.yaml && \
    # Create home directory
    mkdir /flactwomp3 && \
    # Create symlinks
    ln -s /flactwomp3/config /config && \
    ln -s /flactwomp3/flac /flac && \
    ln -s /flactwomp3/logs /logs && \
    ln -s /flactwomp3/mp3 /mp3 && \
    ln -s /flactwomp3/tags /tags && \
    ln -s /flactwomp3/wav /wav

# Copy the files into the container
COPY build/flactwomp3.sh /usr/bin/flactwomp3.sh
COPY build/entrypoint.sh /usr/bin/entrypoint.sh
COPY flactwomp3.yaml /etc/flactwomp3/config.yaml
COPY flactwomp3 /usr/bin/flactwomp3
COPY --from=mp3gain /mp3gain /usr/bin/mp3gain

# Volumes
VOLUME [ "/config", "/flac", "/logs", "/mp3", "/tags", "/wav" ]

# Command to execute when the container is created
CMD [ "/usr/bin/entrypoint.sh" ]
