#!/usr/bin/env sh
#################################################
# Authors
#   Coloc Malakof <colocmalakof@gmail.com>
# Documentation
#   https://gitlab.com/colocmalakof/flactwomp3
#################################################

# Exit immediately if a command exits with a non-zero exit status
set -e

# Treat unset variables as an error when substituting
set -u

# Constants
HOME_DIRECTORY="/flactwomp3"

CONFIG_DIRECTORY="${HOME_DIRECTORY}/config"

echo "Moving the flactwomp3' config file to ${CONFIG_DIRECTORY} if needed..."
if [[ ! -f "${CONFIG_DIRECTORY}/flactwomp3.yaml" ]]; then
    cp "/etc/flactwomp3/config.yaml" "${CONFIG_DIRECTORY}/flactwomp3.yaml"
fi

echo "Starting flactwomp3..."
flactwomp3 --config="${CONFIG_DIRECTORY}/flactwomp3.yaml"
