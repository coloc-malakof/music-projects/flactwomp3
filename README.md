# `flactwomp3`

A quick and dirty multi-threaded Python script to convert `flac` files to `mp3` files.

## Operations

The following operations are available:

- Decode flac files\
  Decode `flac` files to `wav` files using `flac` command.
- Encode wav files\
  Encode the decoded `wav` files to `mp3` files using `lame` command.
- Add ReplayGain\
  Add ReplayGain using `mp3gain` to `mp3` files.
- Tag mp3 files\
  Tag `mp3` files using `eyeD3` command.
- Add comment tag\
  Add a comment tag to the `mp3` files using `eyeD3` command.
- Remove useless tags\
  Remove *"useless"* tags added from `mp3gain` command.
- Convert tags\
  Make sure all tags are encoded in `UTF-8` and `ID3v2.4`.
- Move cover art\
  Move the cover art from the source to the destination.
- New cover art name\
  Optionnaly rename the cover art from the source directory to the destination directory.
- Remove flac files\
  Allow to remove the `flac` files after processing.
- Remove wav files\
  Allow to remove the `wav` files after processing.
- Remove lyrics files\
  Allow to remove the lyrics files after processing.
- Remove mp3 files\
  Allow to remove the `mp3` files after processing (for debug propose).
- Remove cover files from source\
  Allow to remove the cover files from source after processing.
- Remove cover files from destination\
  Allow to remove the cover files from destination after processing (for debug propose).
- Ignore if mp3 directory exists\
  Ignore the processing if the destination directory exists.
- Rename path with encoding preset\
  Allow to rename the path of source directory to destination directory with the encoding preset used.
- Path pattern to replace\
  The path pattern to replace with the audio format used.

## Configuration file

How `flactwomp3` processes files are specified in a `.yaml` configuration file.

If no configuration file is specified from the command line argument `--config`, the default [`flactwomp3.yaml`](https://gitlab.com/colocmalakof/flactwomp3/blob/master/flactwomp3.yaml) in the same directory as the Python script `flactwomp3` will be used (if found).

## Usage

### Standalone

The following programs must be installed for `flactwomp3` to work correctly:

- `flac`
- `metaflac`
- `lame`
- `mp3gain`
- `eyeD3`

```
flactwomp3 [-h] [--config CONFIG_FILE]

optional arguments:
  -h, --help            show this help message and exit
  --config CONFIG_FILE  the YAML configuration file to use.
```

### `docker`

`docker` images are hosted on GitLab and can be pulled directly from there:

```
docker pull registry.gitlab.com/colocmalakof/flactwomp3:latest
```

Then the container can be ran mapping the different volumes on the host computer:

```
docker container run -v /tmp/flactwomp3/config:/config -v /tmp/flactwomp3/flac:/flac -v /tmp/flactwomp3/logs:/logs -v /tmp/flactwomp3/lyrics:/lyrics -v /tmp/flactwomp3/mp3:/mp3 -v /tmp/flactwomp3/wav:/wav registry.gitlab.com/colocmalakof/flactwomp3
```

## Technical details

### Encoding presets

Two encoding presets are avaiable:

- '`V0`' using the following arguments:\
  `lame -T -q 0 --noreplaygain --nogap -V 0`
- '`320`' using the following arguments:\
  `lame -T -q 0 --noreplaygain --nogap --cbr -b 320`